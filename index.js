/**
 * @format
 */

import {AppRegistry, LogBox} from 'react-native';
import App from './src';
import {name as appName} from './app.json';


//sconsole.disableYellowBox = true;
LogBox.ignoreAllLogs(true);

AppRegistry.registerComponent(appName, () => App);
