import Profile from './index';
import {createStackNavigator} from 'react-navigation-stack';
import {
  setHiddenHeaderLayout,
  setDefaultHeaderLayout,
} from '../../../routes/headerUtils';
import appStyles from '../../../styles';

export const ROUTE_NAMES = {
    PROFILE: ' ',
};
  
const RootStack = createStackNavigator({
    [ROUTE_NAMES.PROFILE]: {
      screen: Profile,
      navigationOptions: ({ navigation }) => setHiddenHeaderLayout(navigation),
    },
  },
);

RootStack.navigationOptions = ({ navigation }) => {
  const tabBarVisible = navigation.state.index <= 0;

  return {
    tabBarVisible,
  };
};

export default RootStack;

