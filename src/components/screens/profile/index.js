// @flow
import React, { Component } from 'react';
import {
  TouchableOpacity,
  Linking,
  Platform,
  ScrollView,
  View,
  Text,
  Animated,
  Alert,
  Image,
  StyleSheet
} from 'react-native';

import ButtonContent from '../profile/components/ButtonContent';
import { DefaultText } from '../profile/components/Common';
import styled from 'styled-components';
import LinearGradient from 'react-native-linear-gradient';
import { Creators as ProfileActions } from '../../../store/ducks/authentication';
import {
  getItemFromStorage,
  persistItemInStorage,
} from '../../../utils/AsyncStoarageManager';
import { GoogleSignin } from '@react-native-community/google-signin';
import { LoginManager } from 'react-native-fbsdk'

import CONSTANTS from '../../../utils/CONSTANTS';
import { dateToStringFormat } from '../../../utils/DateUtils';

const Container = styled(View)`
  width: 100%;
  height: 100%;
`;

const ButtonsContainer = styled(View)`
  height: 50%;
  justify-content: center;
  margin-vertical: 2px;
`;

type Props = {
  navigation: Object,
  requestProfile: Function,
  requestUpdateProfile: Function,
  requestLogout: Function,
  authentication: Object,
};

const initialState = {
  profile: {},
  persistedToken: '',
  isLogout: false,
  error: false,
  isBackgroundImageLoaded: false,
};

const BackgroundImage = styled(Image).attrs({
  source: { uri: 'background_pagine' },
  resizeMode: 'cover',
})`
  position: absolute;
  width: 100%;
  height: 100%;
`;

class ProfileComponent extends Component<Props, State> {
  state = initialState;

  _logoutButtonAnimation = new Animated.Value(0);
  _editButtonAnimation = new Animated.Value(0);

  async componentDidMount() {
    Animated.stagger(100, [
      Animated.timing(this._logoutButtonAnimation, {
        toValue: 1,
        duration: 350,
        useNativeDriver: true,
      }),
      Animated.timing(this._editButtonAnimation, {
        toValue: 1,
        duration: 350,
        useNativeDriver: true,
      }),
    ]).start();

    // get profile from storage
    
    const profile = JSON.parse(await getItemFromStorage(CONSTANTS.USER_PROFILE, {}));
  
    const persistedToken = await getItemFromStorage(CONSTANTS.ACCESS_TOKEN, '');

    this.setState({
      profile,
      persistedToken,
    });
  }

  async UNSAFE_componentWillReceiveProps(nextProps) {
    const { authentication } = nextProps;
    const { navigation } = this.props;
    const { isLogout } = this.state;
    console.log(authentication);

    if (authentication.loading || !isLogout) return;

    await persistItemInStorage(CONSTANTS.ACCESS_TOKEN, '');

    navigation.navigate(ROUTE_NAMES.LOGIN);
  }

  async onPressLogout() {    
    const { navigation, requestLogout } = this.props;
    const { profile } = this.state;
    
    if(profile.tipoUtente === 'google'){
      await GoogleSignin.signOut();
    }
    if(profile.tipoUtente === 'facebook'){
      await LoginManager.logOut()
    }
   
    await persistItemInStorage(CONSTANTS.ACCESS_TOKEN, '');   
    await persistItemInStorage(CONSTANTS.TIPO_UTENTE, '');
    navigation.navigate(CONSTANTS.ROUTE_LOGIN);
  }

  async refresh() {
    // get profile from storage
    const profile = JSON.parse(
      await getItemFromStorage(CONSTANTS.USER_PROFILE, {}),
    );

    // console.log('refresh.profile '+JSON.stringify(profile));

    this.setState({
      profile,
    });
  }


  render() {
    const {isBackgroundImageLoaded, profile} = this.state;
    

    return (
      <Container>
        <BackgroundImage onLoad={this.onLoadBackgroundImage}/>     
        <View style={{flexDirection: 'column', justifyContent: 'center', height: '100%', width: '100%', }}> 
        <View style={{justifyContent: 'space-between', height: '60%', width: '100%'}}>          
          <View style={{paddingHorizontal: 20}}>
          
            <View style={{flexDirection: 'column', width: '100%', alignItems: 'center'}}>
              <Text style={styles.textLabel}>NOME: </Text><Text style={styles.textLabel1}>{profile.firstName}</Text>
            </View>    
            <View style={{flexDirection: 'column', width: '100%', alignItems: 'center'}}>
              <Text style={styles.textLabel}>COGNOME: </Text><Text style={styles.textLabel1}>{profile.lastName}</Text>
            </View>  
            <View style={{flexDirection: 'column', width: '100%', alignItems: 'center'}}>
              <Text style={styles.textLabel}>DATA DI NASCITA: </Text><Text style={styles.textLabel1}>{dateToStringFormat(profile.dataDiNascita, CONSTANTS.DEFAULT_DATE_FORMAT)}</Text>
            </View> 
            <View style={{flexDirection: 'column', width: '100%', alignItems: 'center'}}>
              <Text style={styles.textLabel}>PROVINCIA: </Text><Text style={styles.textLabel1}>{profile.provincia}</Text>
            </View> 
            <View style={{flexDirection: 'column', width: '100%', alignItems: 'center'}}>
              <Text style={styles.textLabel}>CITTÀ: </Text><Text style={styles.textLabel1}>{profile.citta}</Text>
            </View> 
            <View style={{flexDirection: 'column', width: '100%', alignItems: 'center'}}>
              <Text style={styles.textLabel}>TITOLO DI STUDIO: </Text><Text style={styles.textLabel1}>{profile.titoloDiStudio}</Text>
            </View> 
            <View style={{flexDirection: 'column', width: '100%', alignItems: 'center'}}>
              <Text style={styles.textLabel}>E-MAIL: </Text><Text style={styles.textLabel1}>{profile.email}</Text>
            </View> 
          </View>
          <View style={{flexDirection: 'column', width:'100%', height: '80%', alignItems: 'center', marginTop: 30 }}>
          <TouchableOpacity
          style={{width:'60%', backgroundColor: '#004C84',  height: 40, borderRadius: 15, alignItems: 'center', justifyContent: 'center', marginBottom: 10}}
          onPress={() => this.onPressLogout()}
          
          >
            <DefaultText color={'#FFFFFF'}>Logout</DefaultText>
          </TouchableOpacity>
          <LinearGradient colors={['#4FCC75', '#268643']} style={styles.linearGradient}>
          <TouchableOpacity
          
          onPress={() => this.onPressNuovaBancaDati()}
          style={{width:'60%', borderRadius: 15, alignItems: 'center', justifyContent: 'center'}}
          
          >
            <DefaultText color={'#FFFFFF'}>Passa a Premium</DefaultText>
          </TouchableOpacity>
          </LinearGradient>
          
          <TouchableOpacity
          style={{width:'60%', backgroundColor: '#004C84',  height: 40, borderRadius: 15, alignItems: 'center', justifyContent: 'center'}}
          onPress={() => this.onPressNuovaBancaDati()}         
          >
            <DefaultText color={'#FFFFFF'}>Impostazioni App</DefaultText>
          </TouchableOpacity>
          </View>
        </View>
        </View>
      </Container>
    );
  }
}

export default ProfileComponent;

const styles = StyleSheet.create({

  textLabel: {
    fontSize: 20,
    fontWeight: 'bold',
    paddingTop: 10
  },

  textLabel1: {
    fontSize: 20,
    fontWeight: 'bold',
    paddingTop: 10,
    color:'#004C84'
  },

  linearGradient:{
      
    justifyContent: 'center',
    alignItems: 'center',
    width: '60%',
    height: 40,
    borderRadius:15,
    marginBottom: 10
  },
});