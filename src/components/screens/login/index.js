// @flow

import React, { Component } from 'react';
import { TouchableOpacity, Alert, Animated, View, Image, Text, StyleSheet, TextInput,KeyboardAvoidingView} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styled from 'styled-components';
import { FBLoginButton } from './components/FBLoginButton';
import { LoginManager, AccessToken, GraphRequest,GraphRequestManager } from "react-native-fbsdk";
import { GoogleSignin } from '@react-native-community/google-signin';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withNavigation } from 'react-navigation';
import {
  requestLogin,
  requestCheckToken,
  requestProfile,
  requestUpdateProfile,
  requestChangePassword,
  requestLogout,
  requestSocialLogin,
  Creators as AuthActions,
} from '../../../store/ducks/authentication';

import { setClientToken } from '../../../services/api';

import { persistItemInStorage, getItemFromStorage } from '../../../utils/AsyncStoarageManager';
import CONSTANTS from '../../../utils/CONSTANTS';

import ButtonContent from './components/ButtonContent';
import { DefaultText } from './components/Common';
import Input from './components/Input';

import appStyles from '../../../styles';


const Container = styled(View)`
  width: 100%;
  height: 100%;
`;

const BackgroundImage = styled(Image).attrs({
  source: { uri: 'new_background_login' },
  resizeMode: 'cover',
  resizeMethod: 'scale',
})`
  position: absolute;
  width: 100%;
  height: 100%;

`;

const styles = StyleSheet.create({
  container: {
    flex:1,
    marginTop: 230,
    alignItems: 'center',  
    height: '100%',
  },

  backgroundImg:{
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
  },

  bottoneRegistrazione:{   
    color: appStyles.colors.white,
    fontFamily: 'CircularStd-Black',
    fontSize: 17,
  },

  accessoText: {
    color: 'white',
    fontSize: 35,
    marginBottom: 40,
    fontWeight: 'bold'

  }, 

  divInput: {
    justifyContent: 'space-around',
    width: '85%',
    alignItems: 'center',
    marginTop: 20
  },

  button: { 
    backgroundColor: '#004C84',
    width: '65%',
    borderRadius: 11,
    marginVertical: 5,
    height: 40,
    justifyContent: 'center',
    elevation: 10,

  },

  
  button2: { 

    backgroundColor: '#F5B506',
    width: '65%',
    borderRadius: 11,
    marginVertical: 5,
    height: 40,
    justifyContent: 'center',
    elevation: 10,
    marginBottom: 15

  },
  
  buttonText: {

    color: 'white',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 15

  },

  divtextContainer: {

    height: 200,
    marginTop: 10
  },
  divText: {
    flexDirection: 'row',
    textAlign: 'center',
    marginTop: 5,
    height: '15%',
    justifyContent:'space-around',

  },
  text3: {
    fontSize: 20,
    color: 'white',
    borderBottomWidth: 1,
    fontWeight: 'bold'
  },

  text2: {
    fontSize: 15,
    color: '#004C84',
    borderBottomWidth: 1,
    borderBottomColor: 'white',
    textAlign: 'center',
    fontWeight: 'bold',
    textDecorationLine: 'underline'
  },

  text1: {
    fontSize: 20,
    color: 'white',
    borderBottomWidth: 1,
    borderBottomColor: 'white',
    marginLeft: 5,
    fontWeight: 'bold'
  },

  textInput: {
    backgroundColor: 'white',
    width: '80%',
    borderRadius: 11,
    borderColor: '#9BC0DB',
    borderWidth: 1,
    height: 40,
    marginVertical: 5
  },

  buttonInput:{
  
  }

 
});

/*const ButtonIcon = styled(Icon).attrs(({ iconName }) => ({
  name: iconName,
  size: 24,
}))`
  color: ${({ theme }) => theme.colors.defaultWhite};
  margin-left: ${({ iconName }) => (iconName === 'facebook' ? -4 : 0)}px;
`;

const SocialButtonWrapper = styled(View)`
  width: 100%;
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
  padding-horizontal: ${({ theme }) => theme.metrics.largeSize}px;
`;

const SocialButtonsContainer = styled(View)`
  height: 50%;
  justify-content: flex-end;
`;*/



const ButtonsContainer = styled(View)`
  height: 10%;
  justify-content: center;
  margin-vertical: 2px;
`;

const ForgotPasswordContainer = styled(Animated.View)`
  width: 100%;
  justify-content: center;
  align-items: center;
  top: 335px;
  left: 10px;
  right: 51px;
  margin-bottom: 18px;
`;

const ForgotPasswordWrapper = styled(View)`
  flex-direction: row;
`;

const RecoverTextButton = styled(TouchableOpacity)`
  margin-left: 4px;
`;


const createAnimationStyle = (animation: Object): Object => {
  const translateY = animation.interpolate({
    inputRange: [0, 1],
    outputRange: [-5, 0],
  });

  return {
    opacity: animation,
    transform: [
      {
        translateY,
      },
    ],
  };
};


const initialState = {
  email: '',
  password: '',
  errors: {},
  isAuthorized: false,
  isProfile: false,
  isLoading: false,
  error: false,
  isBackgroundImageLoaded: false,
};

type Props = {
  requestLogin: Function,
  requestProfile: Function,
  requestSocialLogin: Function,
  authentication: Object,
  navigation: Object,
};

class LoginComponent extends Component<Props, State> {
  state = initialState;

  _emailInputFieldAnimation = new Animated.Value(0);
  _passwordInputFieldAnimation = new Animated.Value(0);
  _loginButtonAnimation = new Animated.Value(0);
  _forgotPasswordTextAnimation = new Animated.Value(0);

  componentDidMount() {
    Animated.stagger(100, [
      Animated.timing(this._emailInputFieldAnimation, {
        toValue: 1,
        duration: 350,
        useNativeDriver: true,
      }),
      Animated.timing(this._passwordInputFieldAnimation, {
        toValue: 1,
        duration: 350,
        useNativeDriver: true,
      }),
      Animated.timing(this._loginButtonAnimation, {
        toValue: 1,
        duration: 350,
        useNativeDriver: true,
      }),
      Animated.timing(this._forgotPasswordTextAnimation, {
        toValue: 1,
        duration: 350,
        useNativeDriver: true,
      }),
    ]).start();

    this._configureGoogleSignIn();
  }

  async UNSAFE_componentWillReceiveProps(nextProps) {
    const { authentication } = nextProps;
    const { navigation, requestProfile } = this.props;
    console.log(authentication);

    if (authentication.loading) return;

    if (authentication.error && !authentication.passwordOk) {
      Alert.alert('Credenziali errate.');
      return;
    }

    if (authentication.error && authentication.passwordOk) {
      Alert.alert('Email non trovata.');
      return;
    }

    if (authentication.loggedIn) {
      
      await persistItemInStorage(
        CONSTANTS.TIPO_UTENTE,
        authentication.profile.tipoUtente,
      ); 

      await persistItemInStorage(
        CONSTANTS.ACCESS_TOKEN,
        authentication.oAuthToken,
      ); 
        console.log("token: "+authentication.oAuthToken);
      await persistItemInStorage(
        CONSTANTS.USER_ID,
        authentication.profile.userId,
      );
      await persistItemInStorage(
        CONSTANTS.USER_PROFILE,
        JSON.stringify(authentication.profile),
      );

      this.setState({
        isAuthorized: true,
      });

      navigation.navigate(CONSTANTS.ROUTE_MAIN_STACK);     
    } 
    
    if (!authentication.loggedIn && authentication.passwordOk) {  
      Alert.alert(
        'La password temporanea è stata inviata nella mail.',
        '',
        [
          { text: 'OK', onPress: () => this.onToggleModal() },
        ],
        { cancelable: false },
      );
    }

  }

  renderInput = (
    id: String,
    placeholder: string,
    iconName: string,
    type: string,
    autofocus: string,
    style: Object,
  ): Object => (
    <Input
      placeholder={placeholder}
      iconName={iconName}
      style={style}
      type={type}
      autofocus={autofocus}
      onChange={text => this.onInputChange(id, text)}
    />
  );

  onInputChange = (key, value) => {
    this.setState({ [key]: value });
  };

  onPressLogin() {
    const { requestLogin, navigation } = this.props;
    const { email, password } = this.state;
    // const payload = {username: email, password: password};
    // console.log(payload);

    requestLogin(email, password);
    //navigation.navigate(CONSTANTS.ROUTE_MAIN_STACK);
  }

  onLoadBackgroundImage = (): void => {
    this.setState({
      isBackgroundImageLoaded: true,
    });
  };

  async onPressFb() {
    console.log("facebook login");
    const result = await LoginManager.logInWithPermissions([
      'public_profile',
      'email',
    ]);

    if (result.isCancelled) {
      console.log('User cancelled the login process');
      return;
    }

    const data = await AccessToken.getCurrentAccessToken();

    if (!data) {
      alert('Problemi nel recuperare il token.');
      return;
    }

    console.log(`Facebook token: ${data.accessToken}`);
    const accessToken = data.accessToken;
    const userId = data.userID;
    this.setState({
      accessToken,
      userId,
    });

    const infoRequest = new GraphRequest(
      '/me', 
      {
        parameters: {
          'fields': {
              'string' : 'id,first_name,last_name,email,link,gender,locale,picture'
          }
        }
      },
      this.responseInfoCallback
/*      (err, res) => {
        console.log(err, res);
        const{accessToken, userId } = this.state;
        console.log('token'+accessToken);
        console.log('userId'+userId);
        requestFacebookLogin(res.email, accessToken, userId, res.first_name, res.last_name);
      }*/
    );
    // Start the graph request.
    new GraphRequestManager().addRequest(infoRequest).start();          

  }

  responseInfoCallback = (error, result) => {
    const{ requestSocialLogin } = this.props;

    if (error) {
      console.log(error)
      alert('Error fetching data: ' + error.toString());
    } else {
      console.log(result)
      const{ accessToken, userId } = this.state;
      console.log('token '+accessToken);
      console.log('userId '+userId);
      requestSocialLogin(result.email, accessToken, userId, result.first_name, result.last_name, 'facebook');

    }
  }

  _configureGoogleSignIn() {
    GoogleSignin.configure({
      webClientId: "771150357830-e3tgn159abp4ehcscsu3pns27rgo7do5.apps.googleusercontent.com",
      androidClientId:"771150357830-o7dhhdutrsbn36h5k89khgh7dkqjce5g.apps.googleusercontent.com",
      offlineAccess: false,
      
    });
   
  }


  async onPressGoogleLogin() {
    const { requestSocialLogin } = this.props;
    console.log("accesso google");
    const { idToken } = await GoogleSignin.signIn();

    if (!idToken) {
      alert('Problemi nel recuperare il token.');

      return;
    }

    // Create a Google credential with the token
    console.log(`Google token: ${idToken}`);
    const currentUser = await GoogleSignin.getCurrentUser();
    console.log(currentUser);
    const tipo = 'google';

    requestSocialLogin(currentUser.user.email, idToken, currentUser.user.id, currentUser.user.givenName, currentUser.user.familyName, tipo);
  }


  onPressResetPassword() {
    const { requestLogin } = this.props;
    const { email, password } = this.state;

    Alert.alert(
      'Confermi il recupero della password?',
      '',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        { text: 'OK', onPress: () => console.log('Fai lo stesso recupero password che sta nel sito') },
      ],
      { cancelable: false },
    );

  }

  renderForgotPasswordText = (): Object => {
    const forgotPasswordTextAnimationStyle = createAnimationStyle(
      this._forgotPasswordTextAnimation,
    );

    return (
      <ForgotPasswordContainer
        style={forgotPasswordTextAnimationStyle}
      >
        <ForgotPasswordWrapper>
          <DefaultText>Hai dimenticato la Password?</DefaultText>
          <RecoverTextButton>
            <DefaultText
              color={appStyles.colors.red}
              onPress={e => this.onPressResetPassword(e)}
            >
              Recupera qui
            </DefaultText>
          </RecoverTextButton>
        </ForgotPasswordWrapper>
      </ForgotPasswordContainer>
    );
  };


  render() {
    const emailAnimationStyle = createAnimationStyle(
      this._emailInputFieldAnimation,
    );
    const passwordAnimationStyle = createAnimationStyle(
      this._passwordInputFieldAnimation,
    );
    const loginButtonAnimationStyle = createAnimationStyle(
      this._loginButtonAnimation,
    );
    const { requestLogin } = this.props;
    const { loading, error } = requestLogin;
    const { email, password, isBackgroundImageLoaded } = this.state;

  


    return ( 

      <Container>
        <BackgroundImage
          onLoad={this.onLoadBackgroundImage}
        />

        <View style={styles.container}>
        <Text style={{color: '#014B83', fontSize :30, fontWeight:'bold', paddingBottom:10}}>Effettua l'accesso!</Text>
        
        <TouchableOpacity style={styles.button} onPress={() => this.onPressFb()}>
            <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
            <Text style={styles.buttonText} disabled={!email || !password}>Accedi con Facebook</Text>
            <Image style = {{ width: 30, height: 30, marginLeft: 10}} source= {{ uri: 'iconafacebook' ,}}></Image>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={styles.button2} onPress={() => this.onPressGoogleLogin()}>
            <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
            <Text style={styles.buttonText} disabled={!email || !password}>Accedi con Google</Text>
            <Image style = {{ width: 45, height: 45, marginLeft: 5}} source= {{ uri: 'iconagoogle' ,}}></Image>
            </View>
          </TouchableOpacity>

          <Animated.View
            >
             {this.renderInput(
                'email',
                'E-mail',
                null,
                'emailAddress',
                false,                
              )}
              {this.renderInput(
                'password',
                'Password',
                null,
                'password',
                false,
                
              )}
            </Animated.View>

          <TouchableOpacity style={styles.button}>
            <Text style={styles.buttonText} disabled={!email || !password}
                onPress={() => this.onPressLogin()}>Accedi</Text>
          </TouchableOpacity>
          <View style={styles.divtextContainer}>
            <TouchableOpacity>
              <Text style={styles.text2}>Hai dimenticato la password?</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Container>

    );
  }
}

// export default LoginComponent;

const mapDispatchToProps = dispatch => bindActionCreators(AuthActions, dispatch);

const mapStateToProps = state => ({
  authentication: state.authentication,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withNavigation(LoginComponent));