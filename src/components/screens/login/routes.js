import { Alert } from 'react';
import { NavigationActions } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { setHiddenHeaderLayout } from '../../../routes/headerUtils';


import Index from './index';

export const ROUTE_NAMES = {
  INDEX: ' ',

};

const ROUTES = createStackNavigator({
  [ROUTE_NAMES.INDEX]: {
    screen: Index,
    navigationOptions: ({ navigation }) => setHiddenHeaderLayout(navigation)
  }, 


});

export default ROUTES;
