// @flow

/*import React, { Component } from 'react';
import {
  TouchableOpacity, Alert, Animated, View, Image,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styled from 'styled-components';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withNavigation } from 'react-navigation';
import {
  requestLogin,
  requestCheckToken,
  requestProfile,
  requestUpdateProfile,
  requestChangePassword,
  requestLogout,
  Creators as AuthActions,
} from '../../../../store/ducks/authentication';

import { setClientToken } from '../../../../services/api';

import { persistItemInStorage } from '../../../../utils/AsyncStoarageManager';
import CONSTANTS from '../../../../utils/CONSTANTS';

import ButtonContent from './ButtonContent';
import { DefaultText } from './Common';
import Input from './Input';

import appStyles from '../../../../styles';

const Container = styled(View)`
  width: 100%;
  height: 100%;
`;

const ButtonIcon = styled(Icon).attrs(({ iconName }) => ({
  name: iconName,
  size: 24,
}))`
  color: ${({ theme }) => theme.colors.defaultWhite};
  margin-left: ${({ iconName }) => (iconName === 'facebook' ? -4 : 0)}px;
`;

const SocialButtonWrapper = styled(View)`
  width: 100%;
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
  padding-horizontal: ${({ theme }) => theme.metrics.largeSize}px;
`;

const SocialButtonsContainer = styled(View)`
  height: 50%;
  justify-content: flex-end;
`;

/*const BackgroundImage = styled(Image).attrs({
  source: { uri: 'background_login' },
  resizeMode: 'cover',
})`
  position: absolute;
  width: 100%;
  height: 100%;
  flex: 1;
`;*/
/*
const ForgotPasswordContainer = styled(Animated.View)`
  width: 100%;
  justify-content: center;
  align-items: center;
  top: 235px;
  left: 10px;
  right: 51px;
  margin-bottom: 18px;
`;

const ForgotPasswordWrapper = styled(View)`
  flex-direction: row;
`;

const RecoverTextButton = styled(TouchableOpacity)`
  margin-left: 4px;
`;

const createAnimationStyle = (animation: Object): Object => {
  const translateY = animation.interpolate({
    inputRange: [0, 1],
    outputRange: [-5, 0],
  });

  return {
    opacity: animation,
    transform: [
      {
        translateY,
      },
    ],
  };
};

const initialState = {
  email: '',
  password: '',
  errors: {},
  isAuthorized: false,
  isProfile: false,
  isLoading: false,
  error: false,
};

type Props = {
  requestLogin: Function,
  requestProfile: Function,
  authentication: Object,
  navigation: Object,
};

class LoginComponent extends Component<Props, State> {
  state = initialState;

  _emailInputFieldAnimation = new Animated.Value(0);
  _passwordInputFieldAnimation = new Animated.Value(0);
  _loginButtonAnimation = new Animated.Value(0);
  _loginFacebookButtonAnimation = new Animated.Value(0);
  _loginGooglePlusButtonAnimation = new Animated.Value(0);
  _forgotPasswordTextAnimation = new Animated.Value(0);

  componentDidMount() {
    Animated.stagger(100, [
      Animated.timing(this._emailInputFieldAnimation, {
        toValue: 1,
        duration: 350,
        useNativeDriver: true,
      }),
      Animated.timing(this._passwordInputFieldAnimation, {
        toValue: 1,
        duration: 350,
        useNativeDriver: true,
      }),
      Animated.timing(this._loginButtonAnimation, {
        toValue: 1,
        duration: 350,
        useNativeDriver: true,
      }),
      Animated.timing(this._loginFacebookButtonAnimation, {
        toValue: 1,
        duration: 350,
        useNativeDriver: true,
      }),
      Animated.timing(this._loginGooglePlusButtonAnimation, {
        toValue: 1,
        duration: 350,
        useNativeDriver: true,
      }),
      Animated.timing(this._forgotPasswordTextAnimation, {
        toValue: 1,
        duration: 350,
        useNativeDriver: true,
      }),
    ]).start();
  }

  renderInput = (
    id: String,
    placeholder: string,
    iconName: string,
    type: string,
    autofocus: string,
    style: Object,
  ): Object => (
    <Input
      placeholder={placeholder}
      iconName={iconName}
      style={style}
      type={type}
      autofocus={autofocus}
      onChange={text => this.onInputChange(id, text)}
    />
  );
  onInputChange = (key, value) => {
    this.setState({ [key]: value });
  };

  onPressLogin() {
    const { requestLogin, navigation } = this.props;
    const { email, password } = this.state;*/
    // const payload = {username: email, password: password};
    // console.log(payload);

    //requestLogin(email, password);
    // navigation.navigate(CONSTANTS.ROUTE_MAIN_STACK);
  //}
/*
  onPressResetPassword() {
    const { requestLogin } = this.props;
    const { email, password } = this.state;

    Alert.alert(
      'Confermi il recupero della password?',
      '',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        { text: 'OK', onPress: () => console.log('Ok Pressed') },
      ],
      { cancelable: false },
    );

    // requestLogin(email, password);
  }

  async UNSAFE_componentWillReceiveProps(nextProps) {
    const { authentication } = nextProps;
    const { navigation, requestProfile } = this.props;
    console.log(authentication);

    if (authentication.loading) return;

    if (authentication.error) {
      Alert.alert('Credenziali errate.');
      return;
    }

    if (authentication.loggedIn && !authentication.profileOk) {
      setClientToken(authentication.oAuthToken);

      await persistItemInStorage(
        CONSTANTS.ACCESS_TOKEN,
        authentication.oAuthToken,
      );
      await persistItemInStorage(
        CONSTANTS.USER_ID,
        authentication.profile.userId,
      );

      this.setState({
        isAuthorized: true,
      });

      requestProfile(authentication.profile.userId);
    } else if (authentication.loggedIn && authentication.profileOk) {
      await persistItemInStorage(
        CONSTANTS.USER_PROFILE,
        JSON.stringify(authentication.profile),
      );

      this.setState({
        isProfile: true,
      });

      navigation.navigate(CONSTANTS.ROUTE_MAIN_STACK);
    }
  }

  renderForgotPasswordText = (): Object => {
    const forgotPasswordTextAnimationStyle = createAnimationStyle(
      this._forgotPasswordTextAnimation,
    );

    return (
      <ForgotPasswordContainer
        style={forgotPasswordTextAnimationStyle}
      >
        <ForgotPasswordWrapper>
          <DefaultText>Hai dimenticato la Password?</DefaultText>
          <RecoverTextButton>
            <DefaultText
              color={appStyles.colors.red}
              onPress={e => this.onPressResetPassword(e)}
            >
              Recupera qui
            </DefaultText>
          </RecoverTextButton>
        </ForgotPasswordWrapper>
      </ForgotPasswordContainer>
    );
  };
  renderSocialButton = (text: string, icon: string, color: string): Object => (
    <ButtonContent
      color={color}
    >
      <SocialButtonWrapper>
        <ButtonIcon
          iconName={icon}
        />
        <DefaultText>{text}</DefaultText>
        <View />
      </SocialButtonWrapper>
    </ButtonContent>
  );

  renderSocialButtons = (): Object => {
    const loginFacebookButtonAnimationStyle = createAnimationStyle(
      this._loginFacebookButtonAnimation,
    );

    const loginGooglePlusButtonAnimationStyle = createAnimationStyle(
      this._loginGooglePlusButtonAnimation,
    );

    return (
      <SocialButtonsContainer>
        <Animated.View
          style={loginFacebookButtonAnimationStyle}
        >
          {this.renderSocialButton(
            'Accedi con Facebook',
            'facebook',
            appStyles.colors.facebook,
            this._loginFacebookButtonAnimation,
          )}
        </Animated.View>
        <Animated.View
          style={loginGooglePlusButtonAnimationStyle}
        >
          {this.renderSocialButton(
            'Accedi con Google+',
            'google-plus',
            appStyles.colors.googlePlus,
            this._loginGooglePlusButtonAnimation,
          )}
        </Animated.View>
      </SocialButtonsContainer>
    );
  };

  render() {
    const emailAnimationStyle = createAnimationStyle(
      this._emailInputFieldAnimation,
    );
    const passwordAnimationStyle = createAnimationStyle(
      this._passwordInputFieldAnimation,
    );
    const loginButtonAnimationStyle = createAnimationStyle(
      this._loginButtonAnimation,
    );
    const { requestLogin } = this.props;
    const { loading, error } = requestLogin;
    const { email, password } = this.state;*/

   /* onLoadBackgroundImage = (): void => {
      this.setState({
        isBackgroundImageLoaded: true,
      });
    };
*/




/* 
OVVIAMENTE VA NEL CONTAINER, PRIMA DI ANIMATED VIEW
 <BackgroundImage
            onLoad={this.onLoadBackgroundImage}
          />

*/ /*
    return (
      <Container>
        <View
          style={{ flex: 1 }}
        >
         
          <Animated.View
            style={{ top: 277, left: 51, right: 51 }}
          >
            {this.renderInput(
              'email',
              'E-mail',
              'email-outline',
              'emailAddress',
              true,
              emailAnimationStyle,
            )}
          </Animated.View>
          <Animated.View
            style={{ top: 300, left: 51, right: 51 }}
          >
            {this.renderInput(
              'password',
              'Password',
              'lock-outline',
              'password',
              false,
              passwordAnimationStyle,
            )}
          </Animated.View>
          <Animated.View
            style={{ top: 400, left: 51, right: 51 }}
          >
            <ButtonContent
              color={appStyles.colors.thirdButton}
              disabled={!email || !password}
              onclick={() => this.onPressLogin()}
            >
              <DefaultText
                color={appStyles.colors.secondButton}
              >
                ACCEDI
              </DefaultText>
            </ButtonContent>
          </Animated.View>
          {this.renderForgotPasswordText()}
        </View>
      </Container>
    );
  }
}*/

// export default LoginComponent;
/*
const mapDispatchToProps = dispatch => bindActionCreators(AuthActions, dispatch);

const mapStateToProps = state => ({
  authentication: state.authentication,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withNavigation(LoginComponent));
*/