// @flow

import React from 'react';
import { TextInput, View } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styled from 'styled-components';

import { ContentContainer } from './Common';
import appStyles from '../../../../styles';

const InputWrapper = styled(View)`
  width: 100%;
  height: 100%;
  flex-direction: row;
  align-items: center;
  padding-horizontal: ${({ theme }) => theme.metrics.largeSize}px;
`;

const InputIcon = styled(Icon).attrs(({ iconName }) => ({
  name: iconName,
  size: 22,
}))`
  margin-right: ${({ theme }) => theme.metrics.mediumSize}px;
  color: ${({ theme }) => theme.colors.defaultWhite};
`;

const CustomInput = styled(TextInput).attrs(
  ({
    placeholder, type, theme, autofocus,
  }) => ({

    placeholderTextColor: '#696969',
    selectionColor: theme.colors.defaultWhite,
    underlineColorAndroid: 'transparent',
    secureTextEntry: type === 'password',
    autoCapitalize: 'none',
    textContentType: type,
    autoCorrect: false,
    autoFocus: autofocus,
    placeholder,
  }),
)`
  width: 90%;
  height: 100%;
  font-family: CircularStd-Book;
  font-weight: bold;
  color: ${({ theme }) => 'gray'};
`;

type InputProps = {
  placeholder: string,
  iconName: string,
  type: string,
  autofocus: string,
};

const Input = ({
  placeholder,
  iconName,
  type,
  autofocus,
  onChange,
}: InputProps): Object => (
  <ContentContainer
    color={appStyles.colors.transparentGray}
    style={{height:40, width: '70%', backgroundColor: '#E5E5E5', borderWidth: 2, borderColor: '#BCBCBC'}}
  >
    <InputWrapper>
      <InputIcon
        iconName={iconName}
      />
      <CustomInput
        placeholder={placeholder}
        type={type}
        autofocus={autofocus}
        onChangeText={onChange}
      />
    </InputWrapper>
  </ContentContainer>
);

export default Input;