import Shop from './index';
import {createStackNavigator} from 'react-navigation-stack';
import appStyles from '../../../styles';
import {
  setHiddenHeaderLayout,
  setDefaultHeaderLayout,
} from '../../../routes/headerUtils';

import CONSTANTS from '../../../utils/CONSTANTS'

export const ROUTE_NAMES = {
    SHOP: ' ',
};
  
const RootStack = createStackNavigator({
    [ROUTE_NAMES.SHOP]: {
      screen: Shop,
      navigationOptions: ({ navigation }) => setHiddenHeaderLayout(navigation),

    },
  },
    {
      initialRouteName: ROUTE_NAMES.SHOP,
      mode: Platform.OS === 'ios' ? 'card' : 'modal',
      headerMode: 'screen',
    },
);

RootStack.navigationOptions = ({ navigation }) => {
  const tabBarVisible = navigation.state.index <= 0;

  return {
    tabBarVisible,
  };
};

export default RootStack;

