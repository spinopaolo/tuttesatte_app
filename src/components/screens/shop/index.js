import React, {Component} from 'react';
import { SearchBar } from 'react-native-elements';
import { TouchableOpacity, Button, View,StyleSheet,Animated,FlatList,Image } from 'react-native';
import styled from 'styled-components';

const Container = styled(View)`
  width: 100%;
  height: 100%;
`;

const initialState = {
    state: '',
    search:'',
    isBackgroundImageLoaded: false,
  };

  const BackgroundImage = styled(Image).attrs({
    source: { uri: 'background_pagine' },
    resizeMode: 'cover',
  })`
    position: absolute;
    width: 100%;
    height: 100%;
  `;

class Shop extends React.Component {

    state = initialState;

    updateSearch = (search) => {
        this.setState({ search });
      };
    

    render(){
      const {isBackgroundImageLoaded, search} = this.state;

      return (
        <Container>
          <BackgroundImage onLoad={this.onLoadBackgroundImage}/>
          <View style={{flexDirection: 'column', justifyContent: 'center', alignItems: 'center',marginTop:150}}>
            <SearchBar inputContainerStyle={{backgroundColor: 'white'}}  searchIcon={{backgroundColor: 'white'}} inputStyle={{backgroundColor: 'white'}}  platform='ios' containerStyle={{ width: '80%', height: 40, backgroundColor: 'white', borderWidth: 0, borderRadius: 15, borderBottomColor: 'black', shadowColor: 'black',}} placeholder="Cerca" placeholderTextColor={'black'} onChangeText={this.updateSearch} value={search}/>
          </View>
          <FlatList>
          
          </FlatList>
        </Container>
    ); 
};

}

export default Shop;