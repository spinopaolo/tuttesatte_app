// @flow

import React, { Component } from 'react';
import { StatusBar, View, Image } from 'react-native';

import SplashScreen from 'react-native-splash-screen';
import styled from 'styled-components';

import { withNavigation } from 'react-navigation';

import {
  getItemFromStorage,
  persistItemInStorage,
} from '../../../utils/AsyncStoarageManager';
import CONSTANTS from '../../../utils/CONSTANTS';

const Container = styled(View)`
  flex: 1;
`;

const ScreenWrapper = styled(View)`
  width: ${({ theme }) => theme.metrics.width}px;
  height: ${({ theme }) => theme.metrics.height}px;
`;

const Wrapper = styled(View)`
  width: 100%;
  height: 100%;
`;

const DarkLayer = styled(View)`
  width: 100%;
  height: 100%;
  background-color: ${({ theme }) => theme.colors.darkLayer};
  position: absolute;
`;

const ImageWrapper = styled(Image).attrs(({ image }) => ({
  source: { uri: image },
  resizeMode: 'cover',
}))`
  width: 100%;
  height: 100%;
  position: absolute;
`;

type Props = {
  navigation: Object,
  
};

type State = {
  persistedToken: string,
  onBoardingIntro: boolean,
  userProfile: {},
};

class SplashComponent extends Component<Props, State> {
  state = {
    persistedToken: '',
    userProfile: {},
  };

  async componentDidMount() {
    
    SplashScreen.hide();

    const { navigation } = this.props;

    // get auth token from storage
    const persistedToken = await getItemFromStorage(CONSTANTS.ACCESS_TOKEN, '');
    const tipoUtente = await getItemFromStorage(CONSTANTS.TIPO_UTENTE, '');

     console.log('persistedToken: '+persistedToken);
    // console.log('onBoardingIntro: '+onBoardingIntro);

    if (persistedToken) navigation.navigate(
        CONSTANTS.ROUTE_MAIN_STACK,
      );
    else {
      if(tipoUtente === 'facebook' || tipoUtente === 'google'){
        navigation.navigate(
          CONSTANTS.ROUTE_MAIN_STACK,
        );
      } else {
        navigation.navigate(
          CONSTANTS.ROUTE_LOGIN,
        );
      }
      
    }

    this.setState({
      persistedToken,
    });
  }

  renderContent = (): Object => (
    <ScreenWrapper>
      <Wrapper>
        <ImageWrapper
          image="splash_screen"
        />
        <DarkLayer />
      </Wrapper>
    </ScreenWrapper>
  );

  render() {
    return (
      <Container>
        <StatusBar
          backgroundColor="transparent"
          barStyle="light-content"
          translucent
          animated
        />
        {this.renderContent()}
      </Container>
    );
  }
}

export default withNavigation(SplashComponent);
