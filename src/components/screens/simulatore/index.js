import React, { Component } from 'react';
import {
  TouchableOpacity,
  ScrollView,
  View,
  Button,
  Text,
  Image,
  StyleSheet
} from 'react-native';

import {withNavigation} from 'react-navigation';
import CONSTANTS from '../../../utils/CONSTANTS';
import styled from 'styled-components';

const Container = styled(View)`
  width: 100%;
  height: 100%;
`;

const initialState = {
  profile: {},
  persistedToken: '',
  isLogout: false,
  error: false,
  isBackgroundImageLoaded: false,
};

type Props = {
  navigation: Object,
};

const BackgroundImage = styled(Image).attrs({
  source: { uri: 'background_pagine' },
  resizeMode: 'cover',
})`
  position: absolute;
  width: 100%;
  height: 100%;
`;


class Simulatore extends Component<Props,State> {

  onPressEsercitazione(){
    const { navigation } = this.props;
  
    navigation.navigate(CONSTANTS.ROUTE_ESERCITAZIONE);
  };
  
  onPressSimulazione(){
    const { navigation } = this.props;
  
    navigation.navigate(CONSTANTS.ROUTE_SCEGLISIMULAZIONE);
  };

  onLoadBackgroundImage = (): void => {
    this.setState({
      isBackgroundImageLoaded: true,
    });
  };

  state = initialState;

  render(){ 

   const {isBackgroundImageLoaded} = this.state;
    
    return(
      <View style={styles.containerView}>

        
              <BackgroundImage
                    onLoad={this.onLoadBackgroundImage}/>
     

          <TouchableOpacity style={styles.simulatoreBotton} onPress={() => this.onPressEsercitazione()}>
            <Text style={styles.buttonText}>Esercitazione da banca dati</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.simulatoreBotton2}  onPress={() => this.onPressSimulazione()}>
            <Text style={styles.buttonText}>Simulatore prova concorso</Text>
          </TouchableOpacity>
               
          </View>

    );
    
  
  };

}

export default withNavigation(Simulatore);

const styles = StyleSheet.create({

  containerView: {
    height: '100%',
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  simulatoreBotton2: {
    height: 140,
    width: '60%',
    backgroundColor: 'white',
    marginTop: 50,
    borderRadius: 37,
    elevation: 50
  },


simulatoreBotton: {
  height: 140,
  width: '60%',
  backgroundColor: 'white',
  
  borderRadius: 37,
  elevation: 50
},

buttonText: {

  color: 'black',
  textAlign: 'center',
  fontWeight: 'bold',
  fontSize: 23,
  justifyContent: 'center',
  alignItems: 'center',
  padding:35

},

viewButton:{
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center'
}
})

