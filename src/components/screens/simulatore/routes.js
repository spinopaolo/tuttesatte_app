import Simulatore from './index';
import {createStackNavigator} from 'react-navigation-stack';
import appStyles from '../../../styles';
import {
  setHiddenHeaderLayout,
  setDefaultHeaderLayout,
} from '../../../routes/headerUtils';
import Esercitazione from '../simulatore/components/esercitazione';
import ScegliSimulazione from '../simulatore/components/ScegliSimulazione';
import CONSTANTS from '../../../utils/CONSTANTS';
import AggiuntaBD from '../simulatore/components/AggiuntaBD';
import QuizEsercitazione from '../simulatore/components/QuizEsercitazione';
import QuizSimulazione from '../simulatore/components/QuizSimulazione';

export const ROUTE_NAMES = {
    SIMULATORE: ' ',
    ESERCITAZIONE: 'ESERCITAZIONE',
    SCEGLISIMULAZIONE: 'SCEGLISIMULAZIONE',
    AGGIUNTABD: 'AGGIUNTABD',
    QUIZESERCITAZIONE: 'QUIZESERCITAZIONE',
    QUIZSIMULAZIONE: 'QUIZSIMULAZIONE',
};

  
const RootStack = createStackNavigator({
    [ROUTE_NAMES.SIMULATORE]: {
      screen: Simulatore, 
      navigationOptions: ({ navigation }) => setHiddenHeaderLayout(navigation),
    },
    [ROUTE_NAMES.ESERCITAZIONE]: {
      screen: Esercitazione,
      navigationOptions: ({ navigation }) => setDefaultHeaderLayout(
        navigation, 
        '',
        'CircularStd-Medium',
        30,
        true,
        false,
        null,
        null,
        false,
        appStyles.metrics.getHeightFromDP('10%'),
        'transparent',
        'transparent',
        0,
       
      ),  
    },

   
    [ROUTE_NAMES.SCEGLISIMULAZIONE]: {
      screen: ScegliSimulazione,
      navigationOptions: ({ navigation }) => setDefaultHeaderLayout(
        navigation, 
        '',
        'CircularStd-Medium',
        30,
        true,
        false,
        null,
        null,
        false,
        appStyles.metrics.getHeightFromDP('10%'),
        'transparent',
        'transparent',
        0
        ),

    },

    [ROUTE_NAMES.AGGIUNTABD]: {
      screen: AggiuntaBD,
      navigationOptions: ({ navigation }) => setDefaultHeaderLayout(
        navigation, 
        '',
        'CircularStd-Medium',
        30,
        true,
        false,
        null,
        CONSTANTS.ROUTE_AGGIUNTABD,
        false,
        appStyles.metrics.getHeightFromDP('10%'),
        'transparent',
        'transparent',
        0
      ),  

    },

    [ROUTE_NAMES.QUIZESERCITAZIONE]: {
      screen: QuizEsercitazione,
      navigationOptions: ({ navigation }) => setDefaultHeaderLayout(
        navigation, 
        '',
        'CircularStd-Medium',
        30,
        true,
        false,
        null,
        CONSTANTS.ROUTE_QUIZESERCITAZIONE,
        false,
        appStyles.metrics.getHeightFromDP('35%'),
        appStyles.colors.background,
        appStyles.colors.background,
      ),
    },

    [ROUTE_NAMES.QUIZSIMULAZIONE]: {
      screen: QuizSimulazione,
      navigationOptions: ({ navigation }) => setDefaultHeaderLayout(
        navigation, 
        '',
        'CircularStd-Medium',
        30,
        true,
        false,
        null,
        CONSTANTS.ROUTE_QUIZSIMULAZIONE,
        true,
        65,
        appStyles.colors.background,
        appStyles.colors.background,
      ),  
    },

  },
    {
      initialRouteName: ROUTE_NAMES.SIMULATORE,
      mode: Platform.OS === 'ios' ? 'card' : 'modal',
      headerMode: 'screen',
    },
);

RootStack.navigationOptions = ({ navigation }) => {
  const tabBarVisible = navigation.state.index <= 1; //A quanti livelli di navigazione si porta la tab bar


  return {
    tabBarVisible,
  };


};

export default RootStack;

