import React, {Component} from 'react';
import { SearchBar } from 'react-native-elements';
import { TouchableOpacity,Text, Button, View,StyleSheet,Animated,FlatList,Image } from 'react-native';
import styled from 'styled-components';
import ButtonContent from '../components/ButtonContent';
import { DefaultText } from '../components/Common';


const Container = styled(View)`
  width: 100%;
  height: 100%;
`;

const initialState = {
    state: '',
    search:'',
    isBackgroundImageLoaded: false,
  };

  const BackgroundImage = styled(Image).attrs({
    source: { uri: 'background_pagine' },
    resizeMode: 'cover',
  })`
    position: absolute;
    width: 100%;
    height: 100%;
  `;

class QuizEsercitazione extends React.Component {

    state = initialState;


    onPressPrecedente(){

    };

    onPressSuccessivo(){

    };

    
    render(){
      const {isBackgroundImageLoaded} = this.state;

        const { search } = this.state;
        const numDomanda = 0;
        const totDomande = 0;
        
        
        return (
    <Container>
      
      <Text>Domanda {numDomanda} di {totDomande} </Text>

    

            <FlatList>

        </FlatList>

        <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 20, marginBottom: 30}}>
        <TouchableOpacity
        style={{backgroundColor: '#F5B506', width: 130, height: 50, justifyContent: 'center', alignItems: 'center', borderRadius: 15}}

          onclick={() => this.onPressPrecedente()}



>
<Text style={{color: 'white', fontWeight: 'bold', fontSize: 15}} >Precedente</Text>
</TouchableOpacity>



<TouchableOpacity
style={{backgroundColor: '#F5B506', width: 130, height: 50, justifyContent: 'center', alignItems: 'center', borderRadius: 15}}
onclick={() => this.onPressSuccessivo()}


>
<Text style={{color: 'white', fontWeight: 'bold', fontSize: 15}} >Successivo</Text>
</TouchableOpacity>
</View>



     </Container>


    ); 

};

}

export default QuizEsercitazione;