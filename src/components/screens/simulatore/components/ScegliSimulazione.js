import React, {Component} from 'react';
import { TouchableOpacity, Button, View,StyleSheet,Animated,FlatList, Text,Image,Alert } from 'react-native';
import appStyles from '../../../../styles';
import { SearchBar } from 'react-native-elements';
import CONSTANTS from '../../../../utils/CONSTANTS';
import {withNavigation} from 'react-navigation';



import ButtonContent from '../components/ButtonContent';
import { DefaultText } from '../components/Common';
import styled from 'styled-components';


const Container = styled(View)`
  width: 100%;
  height: 100%;
`;

const ButtonsContainer = styled(View)`
  flex-direction: column;
  
  justify-content: center;
  margin-vertical: 2px;
  width: 100%
`;


const BackgroundImage = styled(Image).attrs({
  source: { uri: 'background_pagine' },
  resizeMode: 'cover',
})`
  position: absolute;
  width: 100%;
  height: 100%;
`;

const Wrapper = styled(View)`
  width: 100%;
  height: 100%;
`;
const initialState = {
  state: '',
  search:'',
  isBackgroundImageLoaded: false,

};

type Props = {
  navigation: Object,
}



class ScegliSimulazione extends React.Component {

  state = initialState;


  updateSearch = (search) => {
    this.setState({ search });
  };

   

    onPressConcorso(id){ //Da passare l'id 
    const { navigation } = this.props;

    Alert.alert(
      "Simulazione",
      "Sei sicuro di voler iniziare una simulazione di prova?",
      [
        {
          text: "Annulla",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "Inizia", onPress: () =>  navigation.navigate(CONSTANTS.ROUTE_QUIZSIMULAZIONE) }
      ],
      { cancelable: false }
    );

   

    };

    render(){

      const {isBackgroundImageLoaded, search} = this.state;      

      const data = [{id:1, name:'Polizia di stato - Allievo agente'},
      {id:2,name:'Polizia di stato - Allievo vice-ispettore'},
      {id:3,name:'Polizia di stato - Ispettore'},
      {id:4, name:'Polizia di stato - Commissario'},
      {id:5, name:'Polizia di stato - Altro'}]

      return(
       <Container >
          <BackgroundImage onLoad={this.onLoadBackgroundImage}/>
          
          <View style={{flexDirection: 'column-reverse', alignItems: 'center', height: '25%'}}>
            <SearchBar inputContainerStyle={{backgroundColor: 'white'}}  searchIcon={{backgroundColor: 'white'}} inputStyle={{backgroundColor: 'white'}}  platform='ios' containerStyle={{  width: '80%', height: 40, backgroundColor: 'white', borderWidth: 0, borderRadius: 15, borderBottomColor: 'black', shadowColor: 'black',}} placeholder="Cerca concorso" placeholderTextColor={'black'} onChangeText={this.updateSearch} value={search}/>
          </View>
          <View style = {{ paddingTop: 20, width: '100%', height: '80%', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', marginLeft: '12%', marginRight: '12%'}}>
           
          <FlatList
            style={{marginTop:5, height: '100%', width: '100%',}}
            renderItem = {({item})=> ( 
              
              <View style={{marginBottom: 5}}>
              <ButtonsContainer>  
                <Animated.View
              >
              <ButtonContent
                          
                color={'white'}
                onclick={() => this.onPressConcorso(item.id)}
                text-align="center">
                <DefaultText color={'#707070'}>{item.name}</DefaultText>
              </ButtonContent> 
                </Animated.View>
              </ButtonsContainer>  
              </View>             
            )}
 
            data = {data}
            showsVerticalScrollIndicator = {false}
            keyExtractor = {item => item.id}     
          />
          
          </View>
        
        </Container>   
        );
     };

}

export default withNavigation(ScegliSimulazione);

const styles = StyleSheet.create({
  topView:{
    backgroundColor: '#27187E',
    marginTop: 20,
    paddingBottom: 10,
    paddingLeft: 40,
    paddingRight: 40,
    
    
  }
});