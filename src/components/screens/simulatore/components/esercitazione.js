import React, {Component} from 'react';
import { TouchableOpacity, Button, View,StyleSheet,Animated,FlatList, Text,Image } from 'react-native';
import appStyles from '../../../../styles';
import { SearchBar } from 'react-native-elements';


import ButtonContent from '../components/ButtonContent';
import { DefaultText } from '../components/Common';
import styled from 'styled-components';
import CONSTANTS from '../../../../utils/CONSTANTS';
import {withNavigation} from 'react-navigation';
import ActionSheet from 'react-native-actionsheet';


const Container = styled(View)`
  width: 100%;
  height: 100%;
`;

const ButtonsContainer = styled(View)`
  flex-direction: column;
  
  justify-content: center;
  margin-vertical: 2px;
  width: 100%
`;


const BackgroundImage = styled(Image).attrs({
  source: { uri: 'background_pagine' },
  resizeMode: 'cover',
})`
  position: absolute;
  width: 100%;
  height: 100%;
`;

const Wrapper = styled(View)`
  width: 100%;
  height: 100%;
`;
const initialState = {
  state: '',
  search:'',
  isBackgroundImageLoaded: false,

};

type Props = {
  navigation: Object,

}



class Esercitazione extends React.Component {

  state = initialState;

  showActionSheet = () => {
    this.ActionSheet.show()
  }

  updateSearch = (search) => {
    this.setState({ search });
  };

    onPressNuovaBancaDati(){
      const { navigation } = this.props;
      navigation.navigate(CONSTANTS.ROUTE_AGGIUNTABD);
    };

    onPressBancaDati(id){ //Da passare l'id 
    const { navigation } = this.props;
    navigation.navigate(CONSTANTS.ROUTE_QUIZESERCITAZIONE);
    };

    editBancaDati(id){
      this.showActionSheet();
    }

    onPressAction(index){
      alert("uo");
    };

    render(){

      const {isBackgroundImageLoaded, search} = this.state;
      const data = [{id:1, name:'Vigili del fuoco 2016'},{id:2,name:'Vigili del fuoco 2017'},{id:3,name:'Vigili del fuoco 2018'},{id:4, name:'Vigili del fuoco 2019'},{id:5, name:'Vigili del fuoco 2020'}]

      return(
        <Container>
          <BackgroundImage onLoad={this.onLoadBackgroundImage}/>
          <ActionSheet
            ref={o => this.ActionSheet = o}
            options={['Annulla','Elimina batteria', 'Rinomina']}
            cancelButtonIndex={2}
            destructiveButtonIndex= {1}
            onPress={(index) => {this.onPressAction(index)}}
          />
          
          <View style={{flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
            <SearchBar inputContainerStyle={{backgroundColor: 'white'}}  searchIcon={{backgroundColor: 'white'}} inputStyle={{backgroundColor: 'white'}}  platform='ios' containerStyle={{ width: '80%', height: 40, backgroundColor: 'white', borderWidth: 0, marginTop:110, borderRadius: 15, borderColor: '#707070', shadowColor: 'black',}} placeholder="Cerca banca dati" placeholderTextColor={'black'} onChangeText={this.updateSearch} value={search}/>
          </View>
                      
          <Text style={{fontWeight: 'bold', marginLeft: 20, fontSize: 20,marginTop:20}}>Le tue banche dati:</Text>

          <View style = {{ paddingTop: 20, width: '100%', height: '50%', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', marginLeft: '12%', marginRight: '12%'}}>
                  
            <FlatList
              style={{marginTop:5, height: '60%', width: '100%'}}
              renderItem = {({item})=> ( 
                
                <View style={{marginBottom: 5}}>
                  <ButtonsContainer>  
                    <Animated.View>
                      <ButtonContent 
                        color={'white'}
                        onclick={() => this.onPressBancaDati(item.id) }
                        onclickHold = {() => this.editBancaDati()}
                        text-align="center" >
                        <DefaultText color={'#707070'}>{item.name}</DefaultText>
                      </ButtonContent> 
                    </Animated.View>
                  </ButtonsContainer>  
                </View>
              )}

              data = {data}
              showsVerticalScrollIndicator = {false}
              keyExtractor = {item => item.id} 
            />
          
          </View>

          <ButtonsContainer style={{marginTop: '10%', marginLeft: '12%', marginRight: '12%', width: '100%'}}>

            <ButtonContent
            color={'#004C84'}
            onclick={() => this.onPressNuovaBancaDati()}
            text-align="center"
            >
              <DefaultText color={'white'}>Crea nuova banca dati</DefaultText>
            </ButtonContent>

          </ButtonsContainer>
        </Container>      
      );
    };
}

export default withNavigation(Esercitazione);

const styles = StyleSheet.create({
  topView:{
    backgroundColor: '#27187E',
    marginTop: 20,
    paddingBottom: 10,
    paddingLeft: 40,
    paddingRight: 40, 
  }
});