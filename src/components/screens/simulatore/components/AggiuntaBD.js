import React, {useState} from 'react';
import { View, Image, TouchableWithoutFeedback, Text, TextInput, TouchableOpacity,StyleSheet,Picker,Animated,Platform,FlatList,Button} from 'react-native';
import {withNavigation} from 'react-navigation';
import styled from 'styled-components';
import ButtonContent from '../components/ButtonContent';
import { DefaultText } from '../components/Common';


const BackgroundImage = styled(Image).attrs({
    source: { uri: 'sfondo_opaco' },
    resizeMode: 'cover',
  })`
    flex:1;
    position: absolute;
    width: 100%;
    height: 100%;
  `;

  const ButtonsContainer = styled(View)`
  flex-direction: column;
  justify-content: center;
  margin-vertical: 2px;
  width: 100%
`;


  const Container = styled(View)`
  width: 100%;
  height: 100%;
`;

  
const initialState = {
    state: '',
    isBackgroundImageLoaded: false,
  };

  


class AggiuntaBD extends React.Component {


  showActionSheet = () => {
    this.ActionSheet.show()
  }
  
    state = initialState;

    onPressCrea(){

    };

    deleteBancaDati(){ //da passare l'id

    };

    handlePressIn(){
      Animated.timing(this.state.pressAction, {
        duration: ACTION_TIMER,
        toValue: 1
    }).start(this.animationActionComplete);

    };

    handlePressOut(){
      Animated.timing(this.state.pressAction, {
        duration: this._value * ACTION_TIMER,
        toValue: 0
    }).start();

    };

    


    render(){

        const {isBackgroundImageLoaded} = this.state;
        const [selectedValue, setSelectedValue] = "aggiungimateria";

        const data = [{id:1, name:'Italiano'},{id:2,name:'Storia'},{id:3,name:'Geografia'},{id:4, name:'Inglese'},{id:5, name:'Filosofia'},{id:6, name:'Matematica'}]


        return(

    <Container>
        <BackgroundImage
            onLoad={this.onLoadBackgroundImage}/>


        <View style={{marginTop:'30%'}}>

            <View style= {styles.picker}>
            <Picker
                selectedValue={selectedValue}
                 style={{ height: '100%', width: '100%' }}
                onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
      >
        <Picker.Item label="Aggiungi materia" value="aggiungimateria" />
        <Picker.Item label="JavaScript" value="js" />
      </Picker>

            </View>

           <View style= {styles.viewAggiunta}>

           <View style={{flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
            <TextInput
                style={{ marginTop: 12,height: 35,width:'80%', borderColor: '#D1D1D1', borderWidth: 1, borderRadius:10, backgroundColor:'#F2F3F4'}}
                placeholder= 'Nome materia'
                />        
      </View>

              <FlatList
                style={{marginTop:5, height: '60%', width: '100%'}}
                renderItem = {({item})=> ( 
                  
                  <View style={{marginBottom: 5, paddingTop:'5%', marginLeft:'10%'}}>
            
                               <DefaultText color={'#707070'}>{item.name}
                               <TouchableOpacity onPress={() => this.deleteBancaDati()}>
                                    <Image style={{marginRight:10, marginLeft:100}} source={require('../../../../../android/app/src/main/res/drawable/trash.png')}></Image>
                                    </TouchableOpacity>
                                    </DefaultText>

                  </View>
                              
                              
                )}

                  data = {data}
                  showsVerticalScrollIndicator = {false}
                  keyExtractor = {item => item.id}
                  
            />



           </View>

           <ButtonsContainer style={{marginTop:'30%',marginLeft: '12%', marginRight: '12%', width: '100%'}}>

<ButtonContent
color={'#F5B506'}
onclick={() => this.onPressNuovaBancaDati()}
text-align="center"

>
<DefaultText color={'white'} fontWeight='bold'>CACATA DI MERDA</DefaultText>
</ButtonContent>

</ButtonsContainer>

</View>
    </Container>

        );
    };
}

export default withNavigation(AggiuntaBD);

const styles = StyleSheet.create({

    picker: {
        height: '6%',
        marginTop: 70,
        marginLeft:'10%',
        marginRight:'10%',
        width: '80%',
        backgroundColor: 'white',
        borderRadius: 30,
        elevation: 50
      },
    
viewAggiunta: {
    height: '40%',
    marginTop: '12%',
    marginLeft:'10%',
    marginRight:'10%',
    width: '80%',
    backgroundColor: 'white',
    borderRadius: 37,
    elevation: 50
  },

  })
  