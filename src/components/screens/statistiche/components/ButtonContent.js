// @flow

import React from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';
import { withNavigation } from 'react-navigation';

import styled from 'styled-components';
import { ContentContainer } from './Common';

type Props = {
  navigation: Object,
  children: Object,
  color: string,
};

const ButtonContent = ({
  navigation,
  children,
  color,
  disabled,
  onclick,
}: Props): Object => (
  <TouchableOpacity
    // onPress={() => navigation.navigate(ROUTE_NAMES.MAIN_STACK)}
    style={{height:'100%'}}
    onPress={onclick}
    disabled={disabled}
    style={disabled ? styles.containerDisabled : styles.containerEnabled}
  >
    <ContentContainer
      color={color}
    >
      {children}
    </ContentContainer>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  containerDisabled: {
    opacity: 0.3,
  },
  containerEnabled: {
    opacity: 1,
  },
});

export default withNavigation(ButtonContent);
