import React, {Component} from 'react';
import { TouchableOpacity, Button, View,StyleSheet,Animated,FlatList,Image } from 'react-native';
import appStyles from '../../../styles';
import { SearchBar } from 'react-native-elements';

import ButtonContent from '../../../components/screens/statistiche/components/ButtonContent';
import { DefaultText } from '../../../components/screens/statistiche/components/Common';
import styled from 'styled-components';
import styles from '../../../styles'

const Container = styled(View)`
  width: 100%;
  height: 100%;
`;

const ButtonsContainer = styled(View)`
  flex-direction: column;
  justify-content: center;
  margin-vertical: 2px;
  width: 100%;
`;

const initialState = {
    state: '',
    search:'',
    isBackgroundImageLoaded: false,

  };

  const BackgroundImage = styled(Image).attrs({
    source: { uri: 'background_pagine' },
    resizeMode: 'cover',
  })`
    position: absolute;
    width: 100%;
    height: 100%;
  `;


class Statistiche extends React.Component {

    state = initialState;


    updateSearch = (search) => {
        this.setState({ search });
      };
    

   
    render(){
      const {isBackgroundImageLoaded, search} = this.state;

      const data = [{id:1, name:'Italiano'},{id:2,name:'Inglese'},{id:3,name:'Geografia'},{id:4, name:'Astronomia'},{id:5, name:'Spagnolo'},{id:4, name:'Filosofia'},{id:4, name:'Storia'}]

        return (
        <Container>
          <BackgroundImage onLoad={this.onLoadBackgroundImage}/>
            <View style={{flexDirection: 'column', justifyContent: 'center', alignItems: 'center',marginTop:150}}>
              <SearchBar inputContainerStyle={{backgroundColor: 'white'}}  searchIcon={{backgroundColor: 'white'}} inputStyle={{backgroundColor: 'white'}}  platform='ios' containerStyle={{ width: '80%', height: 40, backgroundColor: 'white', borderWidth: 0, borderRadius: 15, borderBottomColor: 'black', shadowColor: 'black',}} placeholder="Cerca materia" placeholderTextColor={'black'} onChangeText={this.updateSearch} value={search}/>
            </View>
            <View style = {{ paddingTop: 20, width: '100%', height: '100%', flexDirection: 'column', flex:1, alignItems: 'center', justifyContent: 'center',marginLeft:'4%', marginRight: '13%'}}>
           
              <FlatList
                style={{width:'100%', height:'100%',flex:1 }}
                numColumns={2}
                renderItem = {({item})=> (
                  
                <View style={{marginBottom: 5, height: '100%', width: '40%', margin: 10}}>
                  <TouchableOpacity style={{height: 180, width: '100%', backgroundColor: 'white', alignItems: 'center', elevation: 10, borderRadius: 15, padding: 15}} onclick={() => this.onPressBancaDati(item.id)}>
                    <DefaultText style={{fontWeight: 'bold', fontSize: 18}} color={'#707070'} >{item.name}</DefaultText>
                      <Image style={{marginRight:10, marginLeft:10, marginTop:30}} source={require('../../../../android/app/src/main/res/drawable/percentualeprova.png')}></Image>
                  </TouchableOpacity> 
                </View>                  
                )}
                data = {data}
                showsVerticalScrollIndicator = {false}
                keyExtractor = {item => item.id}               
              />
            </View>
        </Container>
      );
    };
}

export default Statistiche;