import Statistiche from './index';
import {createStackNavigator} from 'react-navigation-stack';
import appStyles from '../../../styles';
import {
  setHiddenHeaderLayout,
  setDefaultHeaderLayout,
} from '../../../routes/headerUtils';

export const ROUTE_NAMES = {
    STATISTICHE: ' ',
};
  
const RootStack = createStackNavigator({
    [ROUTE_NAMES.STATISTICHE]: {
      screen: Statistiche,
      navigationOptions: ({ navigation }) => setHiddenHeaderLayout(navigation),
    },
  },
    {
      initialRouteName: ROUTE_NAMES.STATISTICHE,
      mode: Platform.OS === 'ios' ? 'card' : 'modal',
      headerMode: 'screen',
    },
);

RootStack.navigationOptions = ({ navigation }) => {
  const tabBarVisible = navigation.state.index <= 0;

  return {
    tabBarVisible,
  };
};

export default RootStack;

