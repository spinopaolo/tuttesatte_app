// @flow

import React from 'react';
import { Platform, StatusBar, TouchableOpacity, Image, Text, View} from 'react-native';
import appStyles from '../styles';

import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import CONSTANTS from '../utils/CONSTANTS';
import { NavigationEvents } from 'react-navigation';


const hiddenProps = {
  [CONSTANTS.NAVIGATION_PARAM_HEADER_HAS_DATA_STYLE]: {
    headerTransparent: true,
    headerStyle: {
      backgroundColor: 'transparent',
      borderBottomWidth: 0,
    },
  },
  [CONSTANTS.NAVIGATION_PARAM_HEADER_LOADING_STYLE]: {
    headerTintColor: 'transparent',
    headerTransparent: true,
    headerStyle: {
      backgroundColor: 'transparent',
      borderBottomWidth: 0,
    },
  },
};

export const handleHiddenHeaderStyle = (
  navigation: Object,
  loading: boolean,
  error: boolean,
): void => {
  const hasLoadingHeaderStyleParam = navigation.getParam(
    CONSTANTS.NAVIGATION_PARAM_HEADER_LOADING_STYLE,
    false,
  );

  const hasHasDataHeaderStyleParam = navigation.getParam(
    CONSTANTS.NAVIGATION_PARAM_HEADER_HAS_DATA_STYLE,
    false,
  );

  if (!hasLoadingHeaderStyleParam && loading) {
    navigation.setParams({
      [CONSTANTS.NAVIGATION_PARAM_HEADER_LOADING_STYLE]: true,
      [CONSTANTS.NAVIGATION_PARAM_HEADER_HAS_DATA_STYLE]: false,
    });
  }

  if (!hasHasDataHeaderStyleParam && !loading && !error) {
    navigation.setParams({
      [CONSTANTS.NAVIGATION_PARAM_HEADER_HAS_DATA_STYLE]: true,
      [CONSTANTS.NAVIGATION_PARAM_HEADER_LOADING_STYLE]: false,
    });
  }
};

const getHiddenProps = (navigation: Object): Object => {
  const { params } = navigation.state;

  let props = {
    headerTintColor: 'transparent',
    headerTransparent: true,
    headerStyle: {
      backgroundColor: 'transparent',
      borderBottomWidth: 0,
      
    },
  };

  if (!!params && params[CONSTANTS.NAVIGATION_PARAM_HEADER_LOADING_STYLE]) {
    props = hiddenProps[CONSTANTS.NAVIGATION_PARAM_HEADER_LOADING_STYLE];
  }

  if (!!params && params[CONSTANTS.NAVIGATION_PARAM_HEADER_HAS_DATA_STYLE]) {
    props = hiddenProps[CONSTANTS.NAVIGATION_PARAM_HEADER_HAS_DATA_STYLE];
  }

  return props;
};

export const setHiddenHeaderLayout = (navigation: Object): Object => {
  const props = getHiddenProps(navigation);

  return {
    headerTintColor: appStyles.colors.defaultWhite,
    ...props,
    headerBackTitle: null,
    ...Platform.select({
      android: {
        headerStyle: {
          marginTop: StatusBar.currentHeight,
        },
      },
    }),
  };
};


export const setDefaultHeaderLayout = (
  navigation: Object,
  title: string,
  fontFamily: string = 'CircularStd-Medium',
  fontSize: ?number,
  backIcon: boolean = true,
  plusIcon: boolean = true,
  marginRight: ?number,
  destination: string,
  editIcon: boolean = false,
  headerHeight: ?number = 0,
  backgroundColor: string,
  shadowColor: string,
  elevation: ?number = 0,
): Object => ({
    title,
  
    headerTitleStyle: {
      fontSize: fontSize || appStyles.metrics.navigationHeaderFontSize,
      color: appStyles.colors.defaultWhite,
      fontWeight: undefined,
      fontFamily,
      marginRight: marginRight || appStyles.metrics.getHeightFromDP('20%'),
    },

    headerTintColor: 'transparent',
    headerStyle: {
      shadowColor: shadowColor || 'transparent',
      backgroundColor: backgroundColor || 'transparent',
      borderBottomWidth: 0,
      height: headerHeight,
      elevation: elevation || 0,


      
  shadowOffset: {
    height: 0,
  },
  shadowRadius: 0,
  shadowOpacity: 0,  

    },

    shadowColor: shadowColor || 'transparent',



  headerRight: editIcon ? (
    <TouchableOpacity
      onPress={() => navigation.navigate(destination)}
    >
      <Icons
        color={appStyles.colors.white}
        name="pencil"
        size={30}
      />
    </TouchableOpacity>
  ) : plusIcon ? (
    <TouchableOpacity
      onPress={() => navigation.navigate(destination)}
    >
      <Icons
        color={appStyles.colors.white}
        name="plus"
        size={30}
      />
    </TouchableOpacity>
  ) : (
    ''
  ),

  
  headerLeft: backIcon ? (
    <TouchableOpacity
      onPress={() => navigation.goBack()}
      style={{width:100}}
    >
      <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
       <Image
             source={{ uri : 'back'}}
             style={{ width: 7.5, height: 13.12, marginLeft: 10}}
          />
          <Text style={{fontSize: 20, fontWeight: 'bold', color: '#707070'}}> Indietro </Text>
      </View>
    </TouchableOpacity>
  ) : (
    ''
  ),
  ...Platform.select({
    android: {
      headerStyle: {
        backgroundColor: 'transparent',
        shadowRadius: 0,
        shadowColor: 'transparent',  
        shadowOpacity: 0,  

        marginTop: StatusBar.currentHeight,
        shadowOffset: {
          height: 0,
        },
        shadow:{
          elevation: 0,
        },
      },
    },
  }),
  headerBackTitle: 'Indietro',
  borderBottomWidth: 0,
});