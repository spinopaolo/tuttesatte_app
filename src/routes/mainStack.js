// @flow

import React from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  createAppContainer,
  withNavigation,
} from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import IconBadge from 'react-native-icon-badge';


import SimulatoreRoutes from '../components/screens/simulatore/routes';
import StatisticheRoutes from '../components/screens/statistiche/routes';
import ShopRoutes from '../components/screens/shop/routes';
import ProfileRoutes from '../components/screens/profile/routes';
//import InfoRoutes from '~/components/screens/info/routes';

import isEqualsOrLargestThanIphoneX from '../utils/isEqualsOrLargestThanIphoneX';
import appStyles from '../styles';
//import { getItemFromStorage } from '~/utils/AsyncStoarageManager';
//import CONSTANTS from '~/utils/CONSTANTS';

export const ROUTE_NAMES = {
  SIMULATORE: 'SIMULATORE',
  STATISTICHE: 'STATISTICHE',
  SHOP: 'SHOP',
  PROFILE: 'PROFILE',
  //INFO: 'INFO',
 /* CART: 'CART',
  TICKET: 'TICKET',*/
};

type Props = {
  tintColor: string,
  bookingsCount: number,
};

const styles = StyleSheet.create({
  icon: {
    width: 26,
   height: 26,
  },
 });

const getTabIcon = (icon: string): Object => ({ tintColor }: Props) => (
  <Icon
    color={tintColor}
    name={icon}
    size={25}
  />
);
const ApplicationTabs = createBottomTabNavigator(
  {
    [ROUTE_NAMES.SIMULATORE]: {
      screen: SimulatoreRoutes,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Image
             source={{ uri : 'giallaquiz'}}
             style={{ width: 38, height: 20, tintColor: tintColor }}
          />
         ),
       
        tabBarLabel: 'Simulatore',
      },
    },
    [ROUTE_NAMES.STATISTICHE]: {
      screen: StatisticheRoutes,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Image
             source={{ uri : 'giallastatistiche'}}
             style={{ width: 28, height: 21, tintColor: tintColor }}
          />
         ),
        tabBarLabel: 'Statistiche',
      },
    },
    [ROUTE_NAMES.SHOP]: {
      screen: ShopRoutes,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Image
             source={{ uri : 'giallacarrello'}}
             style={{ width: 18, height: 17, tintColor: tintColor }}
          />
         ),

        tabBarLabel: 'Shop',
      },
    },
    [ROUTE_NAMES.PROFILE]: {
      screen: ProfileRoutes,

      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Image
             source={{ uri : 'giallaprofilo'}}
             style={{ width: 20, height: 20, tintColor: tintColor }}
          />
         ),

        tabBarLabel: 'Profilo',
      },
    },
  },
  {

    initialRouteName: ROUTE_NAMES.SIMULATORE,
    tabBarPosition: 'bottom',
    optimizationsEnabled: true,
    animationEnabled: true,
    swipeEnabled: false,
    lazy: true,
    tabBarOptions: {
      showLabel: true,
      showIcon: true,
      style: {
        paddingBottom: isEqualsOrLargestThanIphoneX() ? 30 : 0,
        backgroundColor: appStyles.colors.white,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 10,
      },
      
      indicatorStyle: {
        backgroundColor: 'transparent',
      },
     
      inactiveTintColor: appStyles.colors.tabBarInactive,
      activeTintColor: appStyles.colors.tabBarActive,
    },
  },
);

const AppContainer = createAppContainer(ApplicationTabs);

export default AppContainer;