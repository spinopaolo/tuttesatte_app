import {
    createSwitchNavigator,
    createAppContainer,
    withNavigation,
  } from 'react-navigation';
  
  import Splash from '../components/screens/splash';
  //import OnboardingIntro from '~/components/screens/onboarding-intro';
  import Login from '../components/screens/login/routes';
  import MainStack from './mainStack';
  import appStyles from '../styles';
  import { setHiddenHeaderLayout } from '../routes/headerUtils';

  
  export const ROUTE_NAMES = {
    SPLASH: 'SPLASH',
    ONBOARDING_INTRO: 'ONBOARDING_INTRO',
    LOGIN: 'LOGIN',
    MAIN_STACK: 'MAIN_STACK',
  };
  
  const InitialStack = createSwitchNavigator(
    {
      [ROUTE_NAMES.SPLASH]: {
        screen: Splash,
      },
      /*[ROUTE_NAMES.ONBOARDING_INTRO]: {
        screen: OnboardingIntro,
      },*/
      [ROUTE_NAMES.LOGIN]: {
        screen: Login,
        navigationOptions: ({ navigation }) => setHiddenHeaderLayout(navigation)
      },
      [ROUTE_NAMES.MAIN_STACK]: {
        screen: MainStack,
      },
    },
    {
      initialRouteName: ROUTE_NAMES.SPLASH,
      screenOptions: {headerShown: false},
    },
  );
  
  const AppContainer = createAppContainer(InitialStack);
  
  export default AppContainer;