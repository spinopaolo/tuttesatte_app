import axios from 'axios';
import config from './config';
import { getItemFromStorage } from '../utils/AsyncStoarageManager';
import CONSTANTS from '../utils/CONSTANTS';

const api = axios.create({
  baseURL: config.res_url,
});

api.interceptors.request.use(
  async (config) => {
    const token = await getItemFromStorage(CONSTANTS.ACCESS_TOKEN, null);
    if (token) config.headers.Authorization = `${token}`;
    //    console.log('******* '+config.headers.Authorization)
    return config;
  },
  (error) => {
    Promise.reject(error);
  },
);

// Set JSON Web Token in Client to be included in all calls
export const setClientToken = (token) => {
  /*api.interceptors.request.use((config) => {
    config.headers.Authorization = `${token}`;
    return config;
  });*/
};


export default api;

