import axios from 'axios';
import config from './config';

const oAuthApi = axios.create({
  baseURL: config.oath_url,
});

export default oAuthApi;
