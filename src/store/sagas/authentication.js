import { call, put } from 'redux-saga/effects';
import base64 from 'react-native-base64';

import { Creators as AuthenticationActions } from '../../store/ducks/authentication';
import api from '../../services/oauth-api';
import apires from '../../services/api';
import config from '../../services/config';

/*export function* requestLogin(action) {
  try {
    const { username, password } = action.payload;
    console.log(`requestLogin username:${username},password:${password}`);

    const params = {
      username,
      password,
    };

    const response = yield call(api.post, '/session', params, {});

    console.log(`response: ${JSON.stringify(response.data)}`);
    yield put(AuthenticationActions.requestLoginSuccess(response.data));
  } catch (err) {
    console.log(err);
    yield put(AuthenticationActions.requestLoginFailure());
  }
}*/

export function* requestLogin(action) {
  try {
    const { email, password } = action.payload;
    console.log(`requestLogin email:${email},password:${password}`);

    const params = {
      email,
      password,
    };

    const response = yield call(api.post, '/login', params, {
      headers: { 'Content-Type': 'application/json' }
    });

    console.log(`response: ${JSON.stringify(response.data)}`);
    yield put(AuthenticationActions.requestLoginSuccess(response.data));
  } catch (err) {
    console.log(err);
    yield put(AuthenticationActions.requestLoginFailure());
  }
}

export function* requestSocialLogin(action) {
  try {
    const { email, token, userID, nome, cognome, tipo } = action.payload;
    console.log(`requestLogin email:${email},token:${token}`);

    const params = {
      email,
      token,
      userID,
      nome,
      cognome,
      tipo,
    };

    const response = yield call(api.post, '/socialLogin', params, {});

    console.log(`response: ${JSON.stringify(response.data)}`);
    yield put(AuthenticationActions.requestSocialLoginSuccess(response.data));
  } catch (err) {
    console.log(err);
    yield put(AuthenticationActions.requestSocialLoginFailure());
  }
}


export function* requestLogout(action) {
  try {
    const { accessToken } = action.payload;
    console.log('requestLogout');

    const authHeader = `Bearer ${accessToken}`;

    const response = yield call(api.delete, '/revoke', {
      headers: { Authorization: authHeader },
    });

    console.log(`response: ${JSON.stringify(response.data)}`);
    yield put(AuthenticationActions.requestLogoutSuccess(response.data));
  } catch (err) {
    console.log(err);
    yield put(AuthenticationActions.requestLogoutFailure());
  }
}
