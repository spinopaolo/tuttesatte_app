import { all, takeLatest } from 'redux-saga/effects';

import { Types as AuthenticationTypes } from '../../store/ducks/authentication';

import {
  requestLogin,
  requestSocialLogin,
} from './authentication';

export default function* rootSaga() {
  return yield all([
    takeLatest(AuthenticationTypes.LOGIN_REQUEST, requestLogin),
    takeLatest(AuthenticationTypes.SOCIAL_LOGIN_REQUEST, requestSocialLogin),
  ]);
}
