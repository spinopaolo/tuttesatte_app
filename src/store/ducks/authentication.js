import Immutable from 'seamless-immutable';

export const Types = {
  LOGIN_REQUEST: 'authentication/LOGIN_REQUEST',
  LOGIN_SUCCESS: 'authentication/LOGIN_SUCCESS',
  LOGIN_FAILURE: 'authentication/LOGIN_FAILURE',
  SOCIAL_LOGIN_REQUEST: 'authentication/SOCIAL_LOGIN_REQUEST',
  SOCIAL_LOGIN_SUCCESS: 'authentication/SOCIAL_LOGIN_SUCCESS',
  SOCIAL_LOGIN_FAILURE: 'authentication/SOCIAL_LOGIN_FAILURE',
  CHECKTOKEN_REQUEST: 'authentication/CHECKTOKEN_REQUEST',
  CHECKTOKEN_SUCCESS: 'authentication/CHECKTOKEN_SUCCESS',
  CHECKTOKEN_FAILURE: 'authentication/CHECKTOKEN_FAILURE',
  REFRESHTOKEN_REQUEST: 'authentication/REFRESHTOKEN_REQUEST',
  REFRESHTOKEN_SUCCESS: 'authentication/REFRESHTOKEN_SUCCESS',
  REFRESHTOKEN_FAILURE: 'authentication/REFRESHTOKEN_FAILURE',
  PROFILE_REQUEST: 'authentication/PROFILE_REQUEST',
  PROFILE_SUCCESS: 'authentication/PROFILE_SUCCESS',
  PROFILE_FAILURE: 'authentication/PROFILE_FAILURE',
  UPDATEPROFILE_REQUEST: 'authentication/UPDATEPROFILE_REQUEST',
  UPDATEPROFILE_SUCCESS: 'authentication/UPDATEPROFILE_SUCCESS',
  UPDATEPROFILE_FAILURE: 'authentication/UPDATEPROFILE_FAILURE',
  RESETPASSWORD_REQUEST: 'authentication/RESETPASSWORD_REQUEST',
  RESETPASSWORD_SUCCESS: 'authentication/RESETPASSWORD_SUCCESS',
  RESETPASSWORD_FAILURE: 'authentication/RESETPASSWORD_FAILURE',
  CHANGEPASSWORD_REQUEST: 'authentication/CHANGEPASSWORD_REQUEST',
  CHANGEPASSWORD_SUCCESS: 'authentication/CHANGEPASSWORD_SUCCESS',
  CHANGEPASSWORD_FAILURE: 'authentication/CHANGEPASSWORD_FAILURE',
  LOGOUT_REQUEST: 'authentication/LOGOUT_REQUEST',
  LOGOUT_SUCCESS: 'authentication/LOGOUT_SUCCESS',
  LOGOUT_FAILURE: 'authentication/LOGOUT_FAILURE',
};

const INITIAL_STATE = Immutable({
  loading: false,
  error: false,
  loggedIn: false,
  profileOk: false,
  passwordOk: false,
  oAuthToken: '',
  refreshToken: '',
  expiresIn: 0,
  profile: {
    firstName: '',
    lastName: '',
    email: '',
    userId: '',
  },
});

export const Creators = {
  requestLogin: (email, password) => ({
    type: Types.LOGIN_REQUEST,
    payload: {
      email,
      password,
    },
  }),
  requestLoginSuccess: data => ({
    type: Types.LOGIN_SUCCESS,
    payload: { data },
  }),
  requestLoginFailure: error => ({
    type: Types.LOGIN_FAILURE,
    payload: { error },
  }),

  requestSocialLogin: (email, token, userID, nome, cognome, tipo) => ({
    type: Types.SOCIAL_LOGIN_REQUEST,
    payload: {
      email,
      token,
      userID,
      nome,
      cognome,
      tipo,
    },
  }),

  requestSocialLoginSuccess: data => ({
    type: Types.SOCIAL_LOGIN_SUCCESS,
    payload: { data },
  }),
  requestSocialLoginFailure: error => ({
    type: Types.SOCIAL_LOGIN_FAILURE,
    payload: { error },
  }),

  requestCheckToken: token => ({
    type: Types.CHECKTOKEN_REQUEST,
    payload: {
      token,
    },
  }),
  requestCheckTokenSuccess: data => ({
    type: Types.CHECKTOKEN_SUCCESS,
    payload: { data },
  }),
  requestCheckTokenFailure: error => ({
    type: Types.CHECKTOKEN_FAILURE,
    payload: { error },
  }),

  requestProfile: userId => ({
    type: Types.PROFILE_REQUEST,
    payload: {
      userId,
    },
  }),
  requestProfileSuccess: data => ({
    type: Types.PROFILE_SUCCESS,
    payload: { data },
  }),
  requestProfileFailure: error => ({
    type: Types.PROFILE_FAILURE,
    payload: { error },
  }),

  requestUpdateProfile: (accessToken, profile) => ({
    type: Types.UPDATEPROFILE_REQUEST,
    payload: {
      accessToken,
      profile,
    },
  }),
  requestUpdateProfileSuccess: data => ({
    type: Types.UPDATEPROFILE_SUCCESS,
    payload: { data },
  }),
  requestUpdateProfileFailure: error => ({
    type: Types.UPDATEPROFILE_FAILURE,
    payload: { error },
  }),

  requestChangePasswordProfile: (accessToken, oldpassword, newpassword) => ({
    type: Types.CHANGEPASSWORD_REQUEST,
    payload: {
      accessToken,
      oldpassword,
      newpassword,
    },
  }),
  requestChangePasswordSuccess: data => ({
    type: Types.CHANGEPASSWORD_SUCCESS,
    payload: { data },
  }),
  requestChangePasswordFailure: error => ({
    type: Types.CHANGEPASSWORD_FAILURE,
    payload: { error },
  }),

  requestResetPassword: accessToken => ({
    type: Types.RESETPASSWORD_REQUEST,
    payload: {
      accessToken,
    },
  }),
  requestResetPasswordSuccess: data => ({
    type: Types.RESETPASSWORD_SUCCESS,
    payload: { data },
  }),
  requestResetPasswordFailure: error => ({
    type: Types.RESETPASSWORD_FAILURE,
    payload: { error },
  }),

  requestLogout: accessToken => ({
    type: Types.LOGOUT_REQUEST,
    payload: {
      accessToken,
    },
  }),
  requestLogoutSuccess: data => ({
    type: Types.LOGOUT_SUCCESS,
    payload: { data },
  }),
  requestLogoutFailure: error => ({
    type: Types.LOGOUT_FAILURE,
    payload: { error },
  }),
};

const authentication = (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case Types.LOGIN_REQUEST:
      return {
        ...state,
        loggedIn: false,
        profileOk: false,
        loading: true,
        passwordOk: false,
      };

    case Types.LOGIN_SUCCESS:
      return {
        oAuthToken: payload.data.chiave_api,
        profile: {
          userId: payload.data.id_utente,
          firstName: payload.data.nome,
          lastName: payload.data.cognome,
          email: payload.data.email,
          dataDiNascita: payload.data.data_di_nascita,
          citta: payload.data.citta,
          provincia: payload.data.provincia,
          titoloDiStudio: payload.data.titolo_di_studio,
          tipoUtente: payload.data.tipo_utente,
        },
        loggedIn: payload.data.error == true ? false: true,
        loading: false,
        error: payload.data.error,
      };

    case Types.LOGIN_FAILURE:
      return {
        ...state,
        loggedIn: false,
        loading: false,
        error: true,
      };

    case Types.SOCIAL_LOGIN_REQUEST:
    return {
      ...state,
      loggedIn: false,
      profileOk: false,
      loading: true,
      passwordOk: false,
    };

    case Types.SOCIAL_LOGIN_SUCCESS:
      return {
        oAuthToken: payload.data.chiave_api,
        profile: {
          userId: payload.data.id_utente,
          firstName: payload.data.nome,
          lastName: payload.data.cognome,
          email: payload.data.email,
          dataDiNascita: payload.data.data_di_nascita,
          citta: payload.data.citta,
          provincia: payload.data.provincia,
          titoloDiStudio: payload.data.titolo_di_studio,
          socialUserId: payload.data.social_user_id,
          tipoUtente: payload.data.tipo_utente,
        },
        loggedIn: payload.data.error == true ? false: true,
        loading: false,
        error: payload.data.error,
      };

    case Types.SOCIAL_LOGIN_FAILURE:
      return {
        ...state,
        loggedIn: false,
        loading: false,
        error: true,
      };

    case Types.CHECKTOKEN_REQUEST:
      return {
        ...state,
        oAuthToken: payload.token,
        loggedIn: false,
        profileOk: false,
        loading: true,
      };

    case Types.CHECKTOKEN_SUCCESS:
      return {
        ...state,
        loggedIn: true,
        loading: false,
        error: false,
      };

    case Types.CHECKTOKEN_FAILURE:
      return {
        ...state,
        loggedIn: false,
        loading: false,
        error: false,
      };

    case Types.PROFILE_REQUEST:
      return {
        ...state,
        profileOk: false,
        loading: true,
      };

    case Types.PROFILE_SUCCESS:
      return {
        ...state,
        profile: payload.data.data,
        profileOk: true,
        loading: false,
        error: false,
      };

    case Types.PROFILE_FAILURE:
      return {
        ...state,
        loading: false,
        error: true,
      };

    case Types.UPDATEPROFILE_REQUEST:
      return {
        ...state,
        profileOk: false,
        loading: true,
      };

    case Types.UPDATEPROFILE_SUCCESS:
      return {
        ...state,
        profile: payload.data.result,
        profileOk: true,
        loading: false,
        error: false,
      };

    case Types.UPDATEPROFILE_FAILURE:
      return {
        ...state,
        loading: false,
        error: true,
      };

    case Types.CHANGEPASSWORD_REQUEST:
      return {
        ...state,
        passwordOk: false,
        loading: true,
      };

    case Types.CHANGEPASSWORD_SUCCESS:
      return {
        ...state,
        passwordOk: true,
        loading: false,
        error: false,
      };

    case Types.CHANGEPASSWORD_FAILURE:
      return {
        ...state,
        loading: false,
        error: true,
      };

    case Types.RESETPASSWORD_REQUEST:
      return {
        ...state,
        passwordOk: false,
        loading: true,
      };

    case Types.RESETPASSWORD_SUCCESS:
      return {
        ...state,
        passwordOk: true,
        loading: false,
        error: false,
      };

    case Types.RESETPASSWORD_FAILURE:
      return {
        ...state,
        loading: false,
        error: true,
      };

    case Types.LOGOUT_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case Types.LOGOUT_SUCCESS:
      return {
        INITIAL_STATE,
      };

    case Types.LOGOUT_FAILURE:
      return {
        ...state,
        loading: false,
        error: true,
      };

    default:
      return state;
  }
};

export default authentication;
