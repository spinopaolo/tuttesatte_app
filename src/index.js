import ApplicationNavigator from './routes';
import React from 'react';
import AppTheme from './styles';
import { Provider } from 'react-redux';
import store from './store';
import { ThemeProvider } from 'styled-components';


const App = (): Object => (

    <ThemeProvider theme={AppTheme}>
    
    <Provider
    store={store}>

    <ApplicationNavigator/>
    </Provider>
    </ThemeProvider>

);

export default App;