import moment from 'moment';
import momentIT from 'moment/locale/it';

export const currentLocaleDate = (): any => {
  moment.locale('it', momentIT);
  return moment().format('LLL');
};

export const currentDateAsString = (format: string): string => {
  moment.locale('it');
  return moment(new Date()).format(format);
};

export const currentDate = (format: string): any => {
  moment.locale('it');
  return moment(new Date())
    .add(2, 'hours')
    .toDate();
  // return moment(new Date()).toDate();
};

export const stringToDate = (date: string, format: string): any => {
  moment.locale('it', momentIT);
  return moment(date, format).toDate();
};

export const dateToStringFormat = (date: any, format: string): any => {
  if(date === null){
    return '';
  }
  moment.locale('it', momentIT);
  return moment(date).format(format);
};

export const dateToString = (date: any): any => {
  moment.locale('it', momentIT);
  return moment(date).format('LLL');
};

export const extractDateFromString = (date: string, format: string): any => {
  moment.locale('it', momentIT);
  const dt = moment(date, format)
    .add(2, 'hours')
    .toDate();
  return moment(dt).format('DD-MM-YYYY');
};

export const extractTimeFromString = (date: string, format: string): any => {
  moment.locale('it', momentIT);
  const dt = moment(date, format)
    .add(2, 'hours')
    .toDate();
  return moment(dt).format('HH:mm');
};

export const udjustDate = (date: any): any => {
  moment.locale('it');
  return moment(date)
    .add(2, 'hours')
    .toDate();
};
